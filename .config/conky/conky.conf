-- Conky, a system monitor https://github.com/brndnmtthws/conky
--
-- This configuration file is Lua code. You can write code in here, and it will
-- execute when Conky loads. You can use it to generate your own advanced
-- configurations.
--
-- Try this (remove the `--`):
--
--   print("Loading Conky config")
--
-- For more on Lua, see:
-- https://www.lua.org/pil/contents.html

conky.config = {
    alignment = 'top_left',
    background = false,
    border_width = 1,
    border_inner_margin = 10,
    border_outer_margin = 0,
    color0 = '000000', -- black
    color1 = 'ffffff', -- white
    color2 = '71715e', -- gray
    color3 = 'fa2772', -- red
    color4 = 'a7e22e', -- green
    color5 = 'e7db75', -- yellow
    color6 = '66d9ee', -- blue
    color7 = 'ae82ff', -- purple
    color8 = '66efd5', -- teal
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    font = 'altmonospace:size=12',
    gap_x = 60,
    gap_y = 60,
    minimum_height = 5,
    minimum_width = 360,
    maximum_width = 360,
    default_graph_height = 50,
    default_graph_width = 176,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'normal',
    own_window_hints = 'sticky',
    own_window_argb_visual = true,
    own_window_argb_value = 64,
    own_window_transparent = false,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    short_units = true,
    update_interval = 1,
    uppercase = false,
    use_spacer = 'none',
    use_xft = true,
}

conky.text = [[
${color grey}Info:$color ${scroll 32 Conky $conky_version - $sysname $nodename $kernel $machine}
$hr
${color grey}Uptime:$color $uptime
${color grey}Frequency (in GHz):$color $freq_g
${color grey}RAM Usage:$color $mem/$memmax - $memperc% ${membar 4}
${color grey}Swap Usage:$color $swap/$swapmax - $swapperc% ${swapbar 4}
${color grey}CPU Usage:$color $cpu% ${cpugraph cpu0 100,100 ffffff ffffff 0.5 -t -l}
${color grey}Processes:$color $processes  ${color grey}Running:$color $running_processes
$hr
${color grey}File systems:
 ${color grey}/ $color${fs_used /}/${fs_size /} ${fs_bar 6 /}
 ${color grey}/data $color${fs_used /data}/${fs_size /data} ${fs_bar 6 /data}
${color grey}Networking:
Up:$color ${upspeed} ${color grey} - Down:$color ${downspeed}
$hr
${color grey}Name              PID     CPU%   MEM%
${color lightgrey} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color lightgrey} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color lightgrey} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color lightgrey} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
]]

conky.text = [[
${texeci 1800 curl 'wttr.in/NYC?format="%l:+%C+%t+(%f)'}
$color6 CPU: ${color1}$cpu% ${goto 186} ${color6}MEM: ${color1}$mem/${color6}$memmax ${color1}($memperc%)
${cpubar cpu0 6,176} ${membar 6,176}
$color1${cpugraph cpu0} ${memgraph}
$hr
${color6}/ ${goto 70}${color1}${fs_used /}/${color6}${fs_size /}${goto 186} ${color1}${fs_bar 6 /}
${color6}/data ${goto 70}${color1}${fs_used /data}/${color6}${fs_size /data}${goto 186} ${color1}${fs_bar 6 /data}
$hr
${color6}UP: ${color1}${upspeedf wlan0}K${goto 186} ${color6}DOWN: ${color1}${downspeedf wlan0}K
${upspeedgraph wlan0} ${downspeedgraph wlan0}
$hr
]]
