set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

" Loading plugins

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'svermeulen/vim-easyclip'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-sleuth'
Plug 'takac/vim-hardtime'
Plug 'OmniSharp/omnisharp-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

source ~/.vim/vimrc
