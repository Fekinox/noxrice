" Vim color file
" Converted from Textmate theme Monokai using Coloration v0.3.2 (http://github.com/sickill/coloration)

"set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

set t_Co=256
let g:colors_name = "fekinox"

hi Cursor 		ctermbg=White	ctermfg=Black
hi Visual 		ctermbg=Gray	ctermfg=White
hi CursorLine 		ctermbg=Gray	ctermfg=White
hi CursorColumn 	ctermbg=Gray	ctermfg=White
hi ColorColumn 		ctermbg=Black	ctermfg=Green
hi LineNr 		ctermbg=Black	ctermfg=Green
hi VertSplit 		ctermbg=Gray	ctermfg=White
hi MatchParen 		ctermbg=Blue	ctermfg=Black cterm=bold
hi StatusLine 		ctermbg=Black	ctermfg=White
hi StatusLineNC 	ctermbg=Black	ctermfg=White
hi Pmenu 	
hi PmenuSel 	
hi IncSearch 	
hi Search 	
hi Directory 	
hi Folded 	
hi SignColumn 	
hi Normal 	
hi Boolean 	
hi Character 	
hi Comment 	
hi Conditional 	
hi Constant 	
hi Define 	
hi DiffAdd 	
hi DiffDelete 	
hi DiffChange 	
hi DiffText 	
hi ErrorMsg 	
hi WarningMsg 	
hi Float 	
hi Function 	
hi Identifier 	
hi Keyword 	
hi Label 	
hi NonText 	
hi Number 	
hi Operator 	
hi PreProc 	
hi Special 	
hi SpecialComment 	
hi SpecialKey 	
hi Statement 	
hi StorageClass 	
hi String 	
hi Tag 	
hi Title 	
hi Todo 	
hi Type 	
hi Underlined 	
hi rubyClass 	
hi rubyFunction 	
hi rubyInterpolationDelimiter 	
hi rubySymbol 	
hi rubyConstant 	
hi rubyStringDelimiter 	
hi rubyBlockParameter 	
hi rubyInstanceVariable 	
hi rubyInclude 	
hi rubyGlobalVariable 	
hi rubyRegexp 	
hi rubyRegexpDelimiter 	
hi rubyEscape 	
hi rubyControl 	
hi rubyClassVariable 	
hi rubyOperator 	
hi rubyException 	
hi rubyPseudoVariable 	
hi rubyRailsUserClass 	
hi rubyRailsARAssociationMethod 	
hi rubyRailsARMethod 	
hi rubyRailsRenderMethod 	
hi rubyRailsMethod 	
hi erubyDelimiter 	
hi erubyComment 	
hi erubyRailsMethod 	
hi htmlTag 	
hi htmlEndTag 	
hi htmlTagName 	
hi htmlArg 	
hi htmlSpecialChar 	
hi javaScriptFunction 	
hi javaScriptRailsFunction 	
hi javaScriptBraces 	
hi yamlKey 	
hi yamlAnchor 	
hi yamlAlias 	
hi yamlDocumentHeader 	
hi cssURL 	
hi cssFunctionName 	
hi cssColor 	
hi cssPseudoClassId 	
hi cssClassName 	
hi cssValueLength 	
hi cssCommonAttr 	
hi cssBraces 	
