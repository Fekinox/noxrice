"
"	vimrc
"

"	SETTINGS

" Shows command as you type it
set showcmd
" Line number
set relativenumber
set number
set ruler
set numberwidth=6
" Tabs
set tabstop=8
set shiftwidth=8
set noexpandtab
set smarttab
" Necessary for lightline
set noshowmode

set listchars=eol:$		" End of line
set listchars+=tab:>-		" Tabs
set listchars+=space:_		" Spaces
set listchars+=trail:~		" Trailing spaces
set listchars+=extends:>	" Right edge
set listchars+=precedes:<	" Left edge

syntax enable
colorscheme default

let g:hardtime_default_on=0
let g:hardtime_showmsg=1
let g:hardtime_allow_different_key=1

let g:tex_flavor = "latex"

inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" lightline

let g:lightline = {
	\ 'colorscheme': 'wombat',
	\ 'active': {
	\	'left': [	[ 'mode', 'paste' ],
	\				[ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
	\	'right':[	[ 'lineinfo' ],
	\				[ 'percent' ],
	\				[ 'fileformat', 'fileencoding', 'filetype', 'charvaluehex' ]	]
	\ },
	\ 'component_function': {
	\	'gitbranch': 'fugitive#head'
	\ },
	\ 'component': {
	\	'charvaluehex': '0x%B'
	\ },
	\ }
"

let $NVIM_PYTHON_LOG_FILE="/tmp/nvim_log"
let $NVIM_PYTHON_LOG_LEVEL="DEBUG"

"
"	MAPPINGS 
"	(<>)

let mapleader="-"
let maplocalleader="_" 
"	Edit vimrc
nnoremap <leader>ev :silent vsplit ~/.vim/vimrc<cr>
"	Reload vimrc
nnoremap <leader>sv :silent source ~/.vim/vimrc<cr>
"	Toggle line numbers
nnoremap <leader>nu :silent set number!<cr>
"	Toggle whitespace visibility
nnoremap <leader>ws :silent set list!<cr>
"	Toggle search highlighting
nnoremap <leader>hl :silent set hlsearch!<cr>
"	Clear current search
nnoremap <leader>cs :silent let @/ = ""<cr>"
"	Set filetype
nnoremap <leader>sf :silent set ft=
"	Select all
nnoremap <c-a>		<esc>ggVG
nnoremap <leader>tt :HardTimeToggle<cr>

"	Exit insert/visual mode
inoremap qe <esc>
vnoremap qe <esc>
"	Close all folds
nnoremap <leader><Space> zM
"	Open/close fold
nnoremap <Space> za

"	Save as root
nnoremap ZK :w !sudo tee %<cr>

"	Wrap selection
vnoremap <leader>" xa"<esc>pa"<esc>
vnoremap <leader>' xa'<esc>pa'<esc>
vnoremap <leader>( xa(<esc>pa)<esc>
vnoremap <leader>[ xa[<esc>pa]<esc>
vnoremap <leader>{ xa{<esc>pa}<esc>

onoremap p i(
onoremap in( :<c-u>normal! f(vi(<cr>
onoremap il( :<c-u>normal! F)vi(<cr>

onoremap an( :<c-u>normal! f(va(<cr>
onoremap al( :<c-u>normal! F)va(<cr>

" Generate markers and move to them
" Move to next marker with shift+enter

"
"	ABBREVIATIONS

"	some abbreviations...

"
"	AUTOCOMMANDS

"	Persistent folds
augroup AutoSaveFolds
	autocmd!
	autocmd BufWinLeave * silent! mkview
	autocmd BufWinEnter * silent! loadview
augroup END

"	Haskell settings
augroup HaskellFiles
	autocmd!
	autocmd FileType haskell set expandtab
augroup END

"   Shellscript settings
augroup ShellScript
	autocmd!
	autocmd FileType sh set shiftwidth=2
	autocmd FileType sh set tabstop=2
	autocmd FileType sh set expandtab
augroup END

augroup Makefile
	autocmd!
	autocmd FileType make set noexpandtab
augroup END

augroup latex
	autocmd!
	autocmd BufWritePost *.tex silent! execute "!pdflatex % >/dev/null 2>&1" | redraw!
augroup END
"
"	OTHER

"
