#
# .zshrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# DEFINITIONS

# Vim mode

bindkey -v
export KEYTIMEOUT=1

HISTFILE=~/.cache/zsh/zshhist
HISTSIZE=10000
SAVEHIST=10000
setopt autocd extendedglob hist_ignore_dups
bindkey -v
zstyle :compinstall filename '/home/feki/.zshrc'

# Loading zsh modules
autoload -Uz compinit && compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION
autoload -Uz colors && colors
autoload -Uz run-help
# Run-help is aliased to man by default, so fix that
unalias run-help
alias zshhelp=run-help

# Prompt configuration

# Special functions for getting Git status

C1=$PRICOLOR
C2=white
C3=grey

TITLE="%{$fg_bold[$C1]%}"
RESET="%{$reset_color%}"
GIT_DIRTY="%{$fg[$C2]%}*$RESET"
GIT_CLEAN=""
GIT_PREFIX="%{$fg[$C1]%}[$RESET"
GIT_SUFFIX="%{$fg[$C1]%}]$RESET"
VIM_PROMPT="${GIT_PREFIX}NORMAL$GIT_SUFFIX"

parse_git_branch() {
	(command git symbolic-ref -q HEAD || command git name-rev --name-only --no-undefined --always HEAD) 2>/dev/null
}

parse_git_dirty() {
	if command git diff-index --quiet HEAD 2>/dev/null; then
		echo "$GIT_CLEAN"
	else
		echo "$GIT_DIRTY"
	fi
}

git_custom_status() {
	local git_where="$(parse_git_branch)"
	[ -n "$git_where" ] && echo "$(parse_git_dirty)$GIT_PREFIX${git_where#(refs/heads/|tags/)}$GIT_SUFFIX"
}

PS1="$TITLE%n@%m$RESET | %D{%F | %R} %(4~|.../%3~|%~)
  > "

function zle-line-init zle-keymap-select {
	RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $(git_custom_status)"
	zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

# ALIASES

eval `genaliases`

# MISC

unsetopt PROMPT_SP

activate_conda() {
	__conda_setup="$('/home/feki/.local/bin/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
	if [ $? -eq 0 ]; then
	    eval "$__conda_setup"
	else
	    if [ -f "/home/feki/.local/bin/miniconda3/etc/profile.d/conda.sh" ]; then
		. "/home/feki/.local/bin/miniconda3/etc/profile.d/conda.sh"
	    else
		export PATH="/home/feki/.local/bin/miniconda3/bin:$PATH"
	    fi
	fi
	unset __conda_setup
}
