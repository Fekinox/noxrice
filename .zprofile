#
# profile
#

[[ -f ~/.dotfiles/.zshrc ]] && source ~/.dotfiles/.zshrc

export TERM="st-256color"
export TERMINAL="st"
export BROWSER="firefox"
export MAN_DISABLE_SECCOMP=0
export EDITOR=nvim
export PRICOLOR="$(cat ~/.local/share/termcolor)"

for d in $(find ~/.local/bin/scripts/ -type d); do
	export PATH="$PATH:$d";
done
export PATH="$PATH:/home/f/bin:/home/f/.local/bin:/home/f/.local/bin/Godot_v3.2.1-stable_mono_x11_64"

export _JAVA_AWT_WM_NONREPARENTING=1

pgrep -x syncthing >/dev/null 2>&1 || setsid syncthing >/dev/null 2>&1
pgrep -x mpd >/dev/null 2>&1 || mpd >/dev/null 2>&1

eval `genaliases`
